<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



    Route::group([ 'namespace' => 'Api'] , function(){

        Route::get('/', function (){
            return   response(['data'=>[],'msg'=>'API SERVICE WORKED','status'=>200], 200);
        });


        Route::group(['prefix' => 'auth' ] , function(){
            Route::post('/register', 'GeneralController@register');
            Route::post('/login', 'GeneralController@login');
        });

        Route::group(['prefix' => 'product' ] , function(){
            Route::get('/{id}', 'GeneralController@productDetail');
        });

        Route::group(['prefix' => 'category' ] , function(){
            Route::get('/', 'GeneralController@categoryList');
        });



        Route::group(['middleware' => 'auth:api' ,'prefix' => 'cart' ] , function(){
            Route::get('/', 'GeneralController@cartList');
            Route::post('/product/{pid}', 'GeneralController@cartAdd');
            Route::delete('/{cID}', 'GeneralController@cartDrop');

        });

        Route::group(['middleware' => 'auth:api' ,'prefix' => 'admin' ] , function(){
            Route::group(['prefix' => 'product' ] , function(){
                Route::get('/', 'GeneralController@productList');
                Route::post('/create', 'GeneralController@productCreate');
                Route::put('/{id}', 'GeneralController@productUpdate');
                Route::delete('/{id}', 'GeneralController@productDelete');

            });

            Route::group(['prefix' => 'category' ] , function(){

                Route::get('/', 'GeneralController@categoryList');
                Route::post('/create', 'GeneralController@categoryCreate');
                Route::put('/{id}', 'GeneralController@categoryEdit');
                Route::delete('/{id}', 'GeneralController@productDelete');

            });



        });




    });
