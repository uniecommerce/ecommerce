<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{


    protected $appends = ['product'];

    public function getProductAttribute(){
        return $this->product();
    }

    public function product()
    {
        $CatId = $this->id;
        $PCS = productCategory::where('category_id',$CatId)->get();

        if($PCS){

            $products = [];
          foreach ($PCS as $PC){

              $products [] = product::find($PC->product_id);

          }



            return $products;
        }

        return  null;
    }


}
