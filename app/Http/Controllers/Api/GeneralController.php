<?php
namespace App\Http\Controllers\Api;

use App\Address;
use App\cart;
use App\Category;
use App\Http\Controllers\Controller;
use App\product;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;


class GeneralController extends Controller
{

    ##########                                                          ##########
    ##############################################################################
    #########################            Auth                #####################
    ##############################################################################
    ##########                                                          ##########
    public function register(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8|confirmed',
        ]);


        if ($validator->fails()) {
            return   response(['data'=>[$validator->errors()],'msg'=>'Unauthorized','status'=>401], 401);
        }


        $user  =  User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'api_token' => Str::random(191),
        ]);



        return   response(['data'=>[$user],'msg'=> $request->name.' Wellcome','status'=>200], 200);


    }

    public function login(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255',
            'password' => 'required|string',
        ]);


        if ($validator->fails()) {
            return   response(['data'=>[$validator->errors()],'msg'=>'Unauthorized','status'=>401], 401);
        }


        $user = User::where('email',$request->email)->first();


        if( Hash::check($request->password, $user->password) ){

            return   response(['data'=>['name'=> $user->name,'token'=> $user->api_token],'msg'=>'authorized','status'=>200], 200);
        }

        return   response(['data'=>[$validator->errors()],'msg'=>'Unauthorized','status'=>401], 401);



    }

    public function AddressAdd(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'title' => 'required|string|max:255',
            'postalCode' => 'required',
            'address' => 'required',
        ]);


        if ($validator->fails()) {
            return   response(['data'=>[$validator->errors()],'msg'=>'Unauthorized','status'=>401], 401);
        }


        $user  =  Address::create([
            'title' => $request->title,
            'postalCode' => $request->postalCode,
            'address' => Hash::make($request->address),
            'api_token' => Str::random(191),
        ]);



        return   response(['data'=>[$user],'msg'=> $request->name.' Wellcome','status'=>200], 200);


    }

    ##########                                                          ##########
    ##############################################################################
    #########################          PRODUCT               #####################
    ##############################################################################
    ##########


    public function productDetail(Request $request,$id)
    {
        $product = product::findOrFail($id);

        return response(['data'=>[$product],'msg'=>'','status'=>200], 200);
    }





    ###########                                                         ##########
    ##############################################################################
    #########################          Cart               #####################
    ##############################################################################
    ##########                                                          ##########

    public function cartList(Request $request)
    {
        $user = $request->user();

        $cart = cart::where('userID',$user->id)->get();

        return  response(['data'=>[$cart],'msg'=>'','status'=>200], 200);;

    }


    public function cartAdd(Request $request,$pid)
    {
        $user = $request->user();


        $product = product::findOrFail($pid)->get();

        $cart = new cart();
        $cart->userID = $user->id;
        $cart->productId = $pid;
        $cart->save();


        return  response(['data'=>[$cart],'msg'=>'','status'=>200], 200);

    }
    public function cartDrop(Request $request,$cID)
    {
        $user = $request->user();

        $cartItem = cart::findOrFail($cID);
        $cartItem->delete();

        return  response(['data'=>[],'msg'=>'Cart item Drop','status'=>200], 200);

    }

    ##########                                                          ##########
    ##############################################################################
    #########################           ADMIN                #####################
    ##############################################################################
    ##########                                                          ##########


    ############## PRODUCT
    public function productList(Request $request)
    {
        $product = product::orderByDesc('created_at')->get();

        return response(['data'=>[$product],'msg'=>'','status'=>200], 200);
    }
    public function productCreate(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'title' => 'required|string|max:255',
            'count' => 'required|integer',
            'price' => 'required|integer',
        ]);


        if ($validator->fails()) {
            return   response(['data'=>[$validator->errors()],'msg'=>'Unauthorized','status'=>401], 401);
        }


        $product = new product();

        $product->title  = $request->title  ;
        $product->count  = $request->count  ;
        $product->price  = $request->price  ;
        $product->description  = $request->description  ;
        $product->images  =  base64_encode(file_get_contents($request->file('image')));
        $product->save();

        return   response(['data'=>[],'msg'=>'','status'=>200], 200);

    }
    public function productUpdate(Request $request,$id)
    {

        $validator = Validator::make($request->all(), [
            'title' => 'string|max:255',
            'count' => 'integer',
            'price' => 'integer',
        ]);


        if ($validator->fails()) {
            return   response(['data'=>[$validator->errors()],'msg'=>'Unauthorized','status'=>401], 401);
        }


        $product = product::findOrFail($id);

        if($request->title){
            $product->title  = $request->title  ;
        }

        if($request->count){
            $product->count  = $request->count  ;
        }

        if($request->price){
            $product->price  = $request->price ;
        }

        if($request->description){
            $product->description  = $request->description  ;
        }



        if($request->image){
            $product->images  =  base64_encode(file_get_contents($request->file('image')));
        }






        $product->save();

        return   response(['data'=>[],'msg'=>'','status'=>200], 200);
    }
    public function productDelete(Request $request,$id)
    {



        $product = product::findOrFail($id);

        $product->delete();

        return   response(['data'=>[],'msg'=>'','status'=>200], 200);

    }

    ############## CATEGORY
    public function categoryCreate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|string|max:255',
        ]);

        if ($validator->fails()) {
            return   response(['data'=>[$validator->errors()],'msg'=>'Unauthorized','status'=>401], 401);
        }

        $cat = new Category();
        $cat->title  = $request->title  ;
        $cat->save();

        return   response(['data'=>[],'msg'=>'','status'=>200], 200);
    }
    public function categoryEdit(Request $request,$id)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|string|max:255',
        ]);

        if ($validator->fails()) {
            return   response(['data'=>[$validator->errors()],'msg'=>'Unauthorized','status'=>401], 401);
        }

        $cat = Category::findOrFail($id);
        $cat->title  = $request->title  ;
        $cat->save();

        return   response(['data'=>[],'msg'=>'','status'=>200], 200);
    }
    public function categoryList(Request $request)
    {
        $item = Category::orderByDesc('created_at')->get();

        return response(['data'=>[$item],'msg'=>'','status'=>200], 200);
    }

}
