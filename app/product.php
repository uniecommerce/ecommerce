<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class product extends Model
{

    protected $appends = ['category'];

    public function getCategoryAttribute(){
        return $this->categories();
    }

    public function categories()
    {
        $productId = $this->id;
        $category = productCategory::where('product_id',$productId)->get();

        if($category){
            return $category;
        }

        return  null;
    }


}
